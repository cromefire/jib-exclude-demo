rootProject.name = "demo"

pluginManagement {
    repositories {
        gradlePluginPortal()
        mavenLocal()
    }
}
